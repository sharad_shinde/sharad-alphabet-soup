package com.demo;
import java.io.*;
import java.util.*;

class AlphabateSoup {


	private int numOfLinesInGrid;
	private int numOfcolumnsInGrid;
	char[][] grid=null;
	private  List<String> words=new ArrayList<String>();
	private int[] x = { -1, -1, -1, 0, 0, 1, 1, 1 };
	private int[] y = { -1, 0, 1, -1, 1, -1, 0, 1 };
	
	private void init() {
		
		try {
			   BufferedReader reader = new BufferedReader(new FileReader("C:\\workspace\\Test\\Demo\\src\\com\\demo\\AlphabetSoup.txt"));
			   
	            String line;
	       
	            if ((line = reader.readLine()) != null) {
	                String[] dimensions = line.split("x");
	                numOfLinesInGrid = Integer.parseInt(dimensions[0]);
	                numOfcolumnsInGrid = Integer.parseInt(dimensions[1]);
	            }
	            
	             grid=new char [numOfLinesInGrid][numOfcolumnsInGrid];

	            // Read grid
	            for (int i = 0; i < numOfLinesInGrid; i++) {
	                line = reader.readLine();
	                String character[]=line.split(" ");
	                for(int j=0;j<numOfcolumnsInGrid;j++) {
	                	grid[i][j]=character[j].charAt(0);
	                }
	            }
	            
	            while((line=reader.readLine())!=null) {
	            	words.add(line);
	            }
	            reader.close();
		 
		} catch (IOException e) {
				e.printStackTrace();
		}
		
	}


	private boolean process(char[][] sampleGrid, int row, int col, String word)
	{

		if (sampleGrid[row][col] != word.charAt(0))
			return false;

		int firstRow = row;
		int firstCol = col;
		
		int len = word.length();

		// Search word in all 8 directions
		// starting from (row, col)
		for (int dir = 0; dir < 8; dir++) {
			// Initialize starting point
			// for current direction
			int k, rd = row + x[dir], cd = col + y[dir];
			//int rd = row + x[dir];
			//int cd = col + y[dir];
			// First character is already checked,
			// match remaining characters
			for (k = 1; k < len; k++) {
				// If out of bound break
				if (rd >= numOfLinesInGrid || rd < 0 || cd >= numOfcolumnsInGrid || cd < 0)
					break;

				// If not matched, break
				if (sampleGrid[rd][cd] != word.charAt(k))
					break;

				
				// Chk if k is last Char
				if(k == (word.length()-1)) {
							System.out.println(word + " " + firstRow + ":" + firstCol + "   " + rd + ":" + cd);
				}
				
				// Moving in particular direction
				rd += x[dir];
				cd += y[dir];
			}
			

			if (k == len)
				return true;
		}
		return false;
	}

	// Searches given word in a given
	// Grid in all 8 directions
	public void patternSearch( String word)
	{
		// Consider every point as starting. point and search given word
		for (int row = 0; row < numOfLinesInGrid; row++) {
			for (int col = 0; col < numOfcolumnsInGrid; col++) {

				process(grid, row, col, word);
			}
		}
	}


	public static void main(String args[])
	{
		AlphabateSoup alphabateSoup=new AlphabateSoup();
		alphabateSoup.init() ;		
		for(String singleWord : alphabateSoup.words) {
			
			alphabateSoup.patternSearch(singleWord);
		}
		
	}
}

